<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/HelloCat Icon.png" />
    <title>HelloCat</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
    <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
    <style>
        input,
        .nav {
            text-decoration: none;
            margin-left: 700px;
            margin-top: 15px;
        }

        a {
            color: black;
        }

        a:hover {
            text-decoration: none;
            color: white;
        }

        .explore:hover {
            color: white;
        }

        .navnav {
            list-style-type: none;
            float: left;
            margin-left: 25px;
        }

        .navbutton:hover {
            background-color: white;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
        }

        .header {
            margin-top: 175px;
        }

        .donation:hover {
            color: black;
        }

        .button {
            color: white;
            background-color: #f0a5a9;
        }

        .button:hover {
            background-color: #f09ea2;
        }
    </style>
</head>

<body style="
      background-image: url(img/Landing\ Page\ Background.png);
      background-size: 100%;
      background-repeat: no-repeat;
      font-family: 'Roboto', sans-serif;
      background-color: #fbeff0;
    ">
    <div class="nav">
        <ul>
            <li class="navnav" style="
            text-decoration: underline;
            font-size: larger;
            margin-top: -4px;
          ">
                <a class="disabled" href="" style="color: black">Home</a>
            </li>
            <li class="navnav"><a href="adopt">Adopt Cat</a></li>
            <li class="navnav"><a href="donation">Donation</a></li>
            <li class="navnav"><a href="aboutus">About Us</a></li>
            <li class="navnav">
                <a href="{{ route('login') }}"><button class="navbutton" style="
                border-radius: 25px;
                margin-top: -5px;
                border: 1.2px solid;
                width: 90px;
                height: 30px;
              ">
                        Sign In
                    </button></a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div class="header">
            <h1 style="font-weight: bolder">SHOW LOVE TO</h1>
            <h1 style="margin-left: 150px; margin-top: 20px; font-weight: bolder">
                OUR UNIVERSE
            </h1>
            <div style="text-align: center; margin-top: 150px">
                <a href="#content">
                    <button class="explore" style="
                background-color: #f0a5a9;
                border: 1.2px solid;
                border-radius: 25px;
                width: 150px;
                height: 50px;
                font-weight: bolder;
              ">
                        Explore Now
                    </button>
                </a>
            </div>
        </div>
        <div id="content" style="
          padding-top: 40px;
          padding-bottom: 40px;
          width: 88%;
          margin: auto;
          padding-left: 30px;
          padding-right: 30px;
          background-color: white;
          border-radius: 10px;
          margin-top: 77px;
          margin-bottom: 15px;
        ">
            <div class="whatwhy" style="font-weight: lighter">
                <h4>What do we do?</h4>
                <p style="margin-top: 30px">
                    &emsp;&emsp;&emsp;Semakin meningkatnya populasi kucing dan banyaknya
                    kucing liar yang terlantar dan melakukan kawin bebas. Hello Cat
                    hadir bertujuan untuk mengedukasi user untuk menyayangi kucing dan
                    memberitahu cara merawat kucing. Tempat yang kami buat juga agar
                    kucing-kucing liar mempunyai tempat penyelamatan. Kami sangat
                    berharap setidaknya forum yang kami buat dapat mengurangi populasi
                    kucing dengan cara steril kucing dan dapat menjadi penyalur antara
                    pecinta kucing dengan kucing-kucing yang sehat dan siap dirawat.
                </p>
                <h4 style="margin-top: 40px">Why do we do?</h4>
                <p style="margin-top: 30px">
                    &emsp;&emsp;&emsp;Hello Cat adalah Website yang dibangun berawal
                    dari sebuah kepedulian kami dengan kucing-kucing liar diluar sana.
                    Kemudian kami merancang sebuah ide dan berharap ide yang kami buat
                    dapat berkembang dan diterima serta mendapat dukungan oleh
                    masyarakat sekitar. Berikut yang ada di dalam website Hello Cat:
                </p>
                <p>
                    1. Artikel edukasi untuk menyayangi kucing dan cara merawat kucing
                </p>
                <p>2. Membangun tempat penyelamatan kucing-kucing liar</p>
                <p>3. Merescure kucing jalanan dan merawatnya di tempat kami</p>
                <p>4. Menerima bantuan berupa donasi dalam bentuk uang</p>
                <p>5. Membuka jasa open adopsi kucing-kucing yang telah kami rawat</p>
                <p>6. Mengurangi populasi kucing dengan cara steril kucing</p>
                <p>7. Memudahkan orang-orang untuk membantu kucing jalanan</p>
                <p>8. Memudahkan bagi pecinta kucing untuk mengadopsi kucing</p>
                <p>
                    9. Menjamin keamanan transaksi dalam website (menghindari penipuan
                    jual beli kucing)
                </p>
            </div>
            <div>
                <h4 style="margin-top: 40px; margin-bottom: 30px">
                    Let's adopt one!
                </h4>
                <div class="row" style="margin-top: 10px; margin: auto; margin-left: 15px">
                @foreach($adopts as $adopsi)
                    <div class="col-md">
                        <a href="{{ route('adopt.show',['adopt' => $adopsi->id]) }}">
                            <div class="card" style="width: 16rem; border: 1.5px solid; border-radius: 10px">
                                <img src="{{url('')}}/{{$adopsi->foto}}" class="card-img-top" style="
                      height: 150px;
                      border-top-left-radius: 10px;
                      border-top-right-radius: 10px;
                    " />
                                <div class="card-body" style="
                      background-color: #f0a5a9;
                      height: 30px;
                      border-bottom-left-radius: 10px;
                      border-bottom-right-radius: 10px;
                    ">
                                    <p class="card-text" style="margin-top: -10px">
                                        {{$adopsi->detail}}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                </div>
                <div class="row" style="margin-top: 10px; margin-top: 20px; margin-left: 805px">
                    <div>
                        <a href="adopt">
                            <button class="button" style="
                    border: 0px solid;
                    border-radius: 25px;
                    width: 110px;
                    height: 30px;
                  ">
                                View more..
                            </button>
                        </a>
                    </div>
                </div>
                <div style="margin-top: 40px">
                    <h4 style="margin-bottom: 30px">Your donation may help!</h4>
                    @foreach($donations as $donasi)
                        <a class="donation" href="donation">
                            <div class="row" style="margin-left: 20px">
                                <div class="col-md">
                                    <div style="
                        width: 850px;
                        border: 1.5px solid;
                        border-radius: 10px;
                        background-color: #fcebec;
                        height: 200px;
                        ">
                                        <h6 style="margin-top: 15px; margin-left: 15px">
                                            <img src="img/Person Icon.png" style="height: 35px; margin-right: 5px" />
                                            {{$donasi->instansi}}
                                        </h6>
                                        <div style="
                            width: 800px;
                            border: 1px solid;
                            border-radius: 7px;
                            margin-left: 30px;
                            background-color: #fbeff0;
                            height: 125px;
                        ">
                                            <p style="
                            margin-top: 10px;
                            margin-left: 10px;
                            font-weight: lighter;
                            ">
                                                {{$donasi->detail == '' ? 'N/A' : $donasi->detail}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <footer class="text-muted">
        <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
            <p>
                Follow us on:
            </p>
            <div style="margin-top: -36px; margin-left: 90px;">
                <p style="font-size: smaller;">
                    <img src="img/ig.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    @hellocat_rescue
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/gmail.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    hellocat.rescue@gmail.com
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/yt.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    HelloCat Rescue
                </p>
            </div>
            <p style="
            text-align: center;
          ">
                @Designed & built by SweetBonanza Team
            </p>
        </div>
    </footer>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script></script>
</body>

</html>