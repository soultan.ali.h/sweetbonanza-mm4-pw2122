<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="img/HelloCat Icon.png" />
        <title>SweetBonanza</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
        <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
        <style>
            input,
            .nav {
                text-decoration: none;
                margin-left: 694px;
                margin-top: 15px;
            }

            a {
                color: black;
            }

            a:hover {
                text-decoration: none;
                color: white;
            }

            .explore:hover {
                color: white;
            }

            .navnav {
                list-style-type: none;
                float: left;
                margin-left: 25px;
            }

            .navbutton:hover {
                background-color: white;
            }

            .disabled {
                pointer-events: none;
                cursor: default;
            }

            .header {
                margin-top: 50px;
            }

            .button {
                color: white;
                background-color: #f0a5a9;
            }

            .button:hover {
                background-color: #f09ea2;
            }
        </style>
    </head>

    <body style="background-image: url(img/Donation\ Background.png); background-size: 100%; background-repeat: no-repeat; font-family: 'Roboto', sans-serif; background-color: #fbeff0;">
        <div class="nav">
            <ul>
                <li class="navnav"><a href="home">Home</a></li>
                <li class="navnav"><a href="adopt">Adopt Cat</a></li>
                <li class="navnav" style="text-decoration: underline; font-size: larger; margin-top: -4px;">
                    <a class="disabled" href="" style="color: black">Donation</a>
                </li>
                <li class="navnav"><a href="aboutus">About Us</a></li>
                <li class="navnav">
                    <a href="{{ route('logout') }}"><button class="navbutton" type="button" style="border-radius: 25px; margin-top: -5px; border: 1.2px solid; width: 90px; height: 30px;">
                            Log Out
                        </button></a>
                </li>
            </ul>
        </div>
        <div class="container">
            <div class="header">
                <h1 style="font-weight: bolder; margin-left: 450px">
                    Spend Your Money To Help
                </h1>
                <h1 style="margin-left: 715px; margin-top: 20px; font-weight: bolder">
                    These Cute Creatures
                </h1>
            </div>
            <div id="content" style="padding-top: 40px; padding-bottom: 40px; width: 88%; margin: auto; padding-left: 30px; padding-right: 30px; background-color: white; border-radius: 10px; margin-top: 84px; margin-bottom: 15px;">
                <div style="font-weight: lighter">
                    <div class="row">
                        <div class="col-md">
                            <div style="width: 917px; border: 1.5px solid; border-radius: 10px; background-color: #fcebec; height: 234px;">
                                <h5 style="margin-top: 15px; margin-left: 15px; text-decoration: underline; text-underline-offset: 3px; text-decoration-thickness: 1.5px; font-weight: bold;">
                                    Open Donation
                                </h5>
                                <form action="{{ route('donation.store') }}" method="POST" enctype="multipart/form-data"> @csrf
                                    <div>
                                        <div class="row" style="margin-left: 15px; margin-top: 25px">
                                            <div>
                                                <label for="instansi">Nama Instansi</label>
                                            </div>
                                            <div style="margin-left: 21px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('instansi') is-invalid @enderror" id="instansi" name="instansi" value="{{ old('instansi') }}" /> @error('instansi')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>

                                            <div style="margin-left: 107px">
                                                <label for="ktp">Foto KTP</label>
                                            </div>
                                            <div style="margin-left: 20px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-top: -15px; margin-left: 5px">
                                                <input type="file" style="margin-left: 10px; font-size: small; width: 349px;" class="@error('ktp') is-invalid @enderror" id="ktp" name="ktp" value="{{ old('ktp') }}" /> @error('ktp')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px">
                                            <div>
                                                <label for="rek">No. Rek./Bank</label>
                                            </div>
                                            <div style="margin-left: 23px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="number" placeholder="" style="margin-left: 10px; width: 100px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('rekening') is-invalid @enderror" id="rekening" name="rekening" value="{{ old('rekening') }}" /> @error('rekening')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                            <div style="margin-left: 7px">
                                                <label>/</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 1px; width: 65px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('bank') is-invalid @enderror" id="bank" name="bank" value="{{ old('bank') }}" /> @error('bank')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                            <div style="margin-left: 107px">
                                                <label for="detail">Detail</label>
                                            </div>
                                            <div style="margin-left: 46px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: 3px">
                                                <textarea placeholder="ceritakan tentang penangkaran kucing kamu.." style="margin-left: 10px; height: 53px; width: 347px; border-radius: 5px; border: 1px solid; font-weight: lighter;" id="detail" rows="3" name="detail">{{ old('detail') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px; margin-top: -30px">
                                            <div>
                                                <label for="nama">Atas Nama</label>
                                            </div>
                                            <div style="margin-left: 44px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" /> @error('nama')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px">
                                            <div>
                                                <label for="kucing">Foto Kucing</label>
                                            </div>
                                            <div style="margin-left: 39px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-top: -15px; margin-left: 5px; margin-right: -40px;">
                                                <input type="file" style="margin-left: 10px; font-size: small; width: 189px;" class="@error('kucing') is-invalid @enderror" id="kucing" name="kucing" value="{{ old('kucing') }}" /> @error('kucing')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: -9px; margin-left: 818px">
                                            <div>
                                                <a href="">
                                                    <button type="submit" class="button" style="border: 0px solid; border-radius: 25px; width: 80px; height: 30px;">
                                                        Submit
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <h4 style="margin-top: 40px; margin-bottom: 30px">
                        Current Running Donation
                    </h4>
                    @foreach($donations as $donasi)
                        <div style="width: 917px; border: 1.5px solid; border-radius: 10px; background-color: #fcebec; height: 260px;">
                            <div style="font-weight: lighter">
                                <div class="row" style="margin-left: 15px; margin-top: 15px">
                                    <div>
                                        <label>Nama Instansi</label>
                                    </div>
                                    <div style="margin-left: 21px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px">
                                        <label style="margin-left: 10px; width: 184px">
                                            {{$donasi->instansi}}
                                        </label>
                                    </div>
                                    <div style="margin-left: 107px">
                                        <label>Detail</label>
                                    </div>
                                    <div style="margin-left: 46px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px; margin-bottom: -112px">
                                        <label style="margin-left: 10px; width: 347px; height: 120px">
                                            {{$donasi->detail == '' ? 'N/A' : $donasi->detail}}
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px">
                                    <div>
                                        <label>No. Rek./Bank</label>
                                    </div>
                                    <div style="margin-left: 23px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 15px">
                                        <label style="width: 100px"> {{$donasi->rekening}} </label>
                                    </div>
                                    <div style="margin-left: 7px">
                                        <label style="width: 3px"> / </label>
                                    </div>
                                    <div style="margin-left: 7px">
                                        <label style="width: 67px"> {{$donasi->bank}} </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px">
                                    <div>
                                        <label>Atas Nama</label>
                                    </div>
                                    <div style="margin-left: 44px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px">
                                        <label style="margin-left: 10px; width: 184px">
                                            {{$donasi->nama}}
                                        </label>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px">
                                    <div>
                                        <label>Foto Kucing</label>
                                    </div>
                                    <div style="margin-left: 39px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-top: 5px; margin-left: 15px; margin-right: -40px;">
                                        <img src="{{url('')}}/{{$donasi->kucing}}" style="width: 125px; height: 125px; border: 1px solid; border-radius: 10px;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-center" style="margin-top: 30px;">
                        <h5 style="font-weight: bolder;">Page</h5>
                        <div class="pagination justify-content-center" style="font-size: 20px;">{{$donations}}</div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="text-muted">
            <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
                <p>
                    Follow us on:
                </p>
                <div style="margin-top: -36px; margin-left: 90px;">
                    <p style="font-size: smaller;">
                        <img src="img/ig.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        @hellocat_rescue
                    </p>
                    <p style="margin-top: -10px; font-size: smaller;">
                        <img src="img/gmail.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        hellocat.rescue@gmail.com
                    </p>
                    <p style="margin-top: -10px; font-size: smaller;">
                        <img src="img/yt.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        HelloCat Rescue
                    </p>
                </div>
                <p style="
                text-align: center;
            ">
                    @Designed & built by SweetBonanza Team
                </p>
            </div>
        </footer>
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script></script>
    </body>

</html>