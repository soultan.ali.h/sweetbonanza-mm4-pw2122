<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/HelloCat Icon.png" />
    <title>HelloCat</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
    <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
    <style>
        input,
        .nav {
            text-decoration: none;
            margin-left: 1053px;
            margin-top: 15px;
        }

        a {
            color: black;
        }

        a:hover {
            text-decoration: none;
            color: white;
        }

        .explore:hover {
            color: white;
        }

        .navnav {
            list-style-type: none;
            float: left;
            margin-left: 25px;
        }

        .navbutton:hover {
            background-color: white;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
        }

        .header {
            margin-top: 175px;
        }

        .donation:hover {
            color: black;
        }

        .button {
            color: white;
            background-color: #f0a5a9;
        }

        .button:hover {
            background-color: #f09ea2;
        }
    </style>
</head>

<body style="
    background-image: url(img/About\ Us_Adopt\ Cat\ Detail\ Background.png);
    background-size: 100%;
    background-repeat: no-repeat;
    font-family: 'Roboto', sans-serif;
    background-color: #fbeff0;
    ">
    <div class="nav">
        <ul>
            <li class="navnav">
                <a href="{{ route('logout') }}"><button class="navbutton" type="button" style="
                border-radius: 25px;
                margin-top: -5px;
                border: 1.2px solid;
                width: 90px;
                height: 30px;
              ">
                        Log Out
                    </button></a>
            </li>
        </ul>
    </div>
    <div>
        <div id="content" style="
          padding-top: 40px;
          padding-bottom: 40px;
          width: 90%;
          margin: auto;
          padding-left: 30px;
          padding-right: 30px;
          background-color: white;
          border-radius: 10px;
          margin-top: 8px;
          margin-bottom: 15px;
        ">
            <h1 style="
            font-weight: bolder;
            text-align: center;
            text-decoration: underline;
            margin-bottom: 75px;
          ">
                Admin Page
            </h1>
            <div>
                <div>
                    <h5 style="text-align: center; font-weight: bolder; margin-bottom: 20px;">Cat Adoption Data</h5>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Pemilik</th>
                                <th>Jenis</th>
                                <th>Umur</th>
                                <th>Kondisi</th>
                                <th>Kontak</th>
                                <th>Foto</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($adopts as $adopsi)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td><a href="{{ route('adopt.show',['adopt' => $adopsi->id]) }}">{{$adopsi->id}}</a></td>
                                <td>{{$adopsi->pemilik}}</td>
                                <td>{{$adopsi->jenis}}</td>
                                <td>{{$adopsi->umur}}</td>
                                <td>{{$adopsi->kondisi == 'T'?'Tidak Sehat':'Sehat'}}</td>
                                <td>{{$adopsi->kontak}}</td>
                                <td><img height="30px" src="{{url('')}}/{{$adopsi->foto}}" class="rounded" alt=""></td>
                                <td>{{$adopsi->detail == '' ? 'N/A' : $adopsi->detail}}</td>
                                <td>
                                    <form action="{{ route('adopt.destroy',['adopt'=>$adopsi->id]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <td colspan="9" class="text-center">Tidak ada data...</td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                
                <div style="margin-top: 50px;">
                    <h5 style="text-align: center; font-weight: bolder; margin-bottom: 20px;">Donation Data</h5>
                    @if(session()->has('pesan'))
                    <div class="alert alert-success">
                        {{ session()->get('pesan') }}
                    </div>
                    @endif
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID</th>
                                <th>Instansi</th>
                                <th>Rekening</th>
                                <th>Bank</th>
                                <th>Nama</th>
                                <th>Foto Kucing</th>
                                <th>Foto KTP</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($donations as $donasi)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td><a href="{{ route('donation.show',['donation' => $donasi->id]) }}">{{$donasi->id}}</a></td>
                                <td>{{$donasi->instansi}}</td>
                                <td>{{$donasi->rekening}}</td>
                                <td>{{$donasi->bank}}</td>
                                <td>{{$donasi->nama}}</td>
                                <td><img height="30px" src="{{url('')}}/{{$donasi->kucing}}" class="rounded" alt=""></td>
                                <td><img height="30px" src="{{url('')}}/{{$donasi->ktp}}" class="rounded" alt=""></td>
                                <td>{{$donasi->detail == '' ? 'N/A' : $donasi->detail}}</td>
                                <td>
                                    <form action="{{ route('donation.destroy',['donation'=>$donasi->id]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger ml-3">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <td colspan="9" class="text-center">Tidak ada data...</td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <footer class="text-muted">
        <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
            <p>
                Follow us on:
            </p>
            <div style="margin-top: -36px; margin-left: 90px;">
                <p style="font-size: smaller;">
                    <img src="img/ig.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    @hellocat_rescue
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/gmail.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    hellocat.rescue@gmail.com
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/yt.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    HelloCat Rescue
                </p>
            </div>
            <p style="
            text-align: center;
          ">
                @Designed & built by SweetBonanza Team
            </p>
        </div>
    </footer>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script></script>
</body>

</html>