<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="img/HelloCat Icon.png" />
    <title>SweetBonanza</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
    <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
    <style>
        input,
        .nav {
            text-decoration: none;
            margin-left: 694px;
            margin-top: 15px;
        }

        a {
            color: black;
        }

        a:hover {
            text-decoration: none;
            color: white;
        }

        .explore:hover {
            color: white;
        }

        .navnav {
            list-style-type: none;
            float: left;
            margin-left: 25px;
        }

        .navbutton:hover {
            background-color: white;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
</head>

<body style="
      background-image: url(img/About\ Us_Adopt\ Cat\ Detail\ Background.png);
      background-size: 100%;
      background-repeat: no-repeat;
      font-family: 'Roboto', sans-serif;
      background-color: #fbeff0;
    ">
    <div class="nav">
        <ul>
            <li class="navnav"><a href="home">Home</a></li>
            <li class="navnav"><a href="adopt">Adopt Cat</a></li>
            <li class="navnav"><a href="donation">Donation</a></li>
            <li class="navnav" style="
            text-decoration: underline;
            font-size: larger;
            margin-top: -4px;
          ">
                <a class="disabled" href="" style="color: black">About Us</a>
            </li>
            <li class="navnav">
                <a href="{{ route('logout') }}"><button class="navbutton" type="button" style="
                border-radius: 25px;
                margin-top: -5px;
                border: 1.2px solid;
                width: 90px;
                height: 30px;
              ">
                        Log Out
                    </button></a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div id="content" style="
          padding-top: 40px;
          padding-bottom: 40px;
          width: 88%;
          margin: auto;
          padding-left: 30px;
          padding-right: 30px;
          background-color: white;
          border-radius: 10px;
          margin-top: 8px;
          margin-bottom: 15px;
        ">
            <h1 style="
            font-weight: bolder;
            text-align: center;
            text-decoration: underline;
            margin-bottom: 75px;
          ">
                About Us
            </h1>
            <div style="font-weight: lighter; margin-left: 137px">
                <div class="row">
                    <div class="col-md">
                        <div style="
                  width: 171px;
                  height: 249px;
                  border: 1.5px solid;
                  border-radius: 10px;
                  background-color: #fcebec;
                ">
                            <img src="img/Oliv.png" style="
                    border-radius: 5px;
                    margin: 12px;
                    height: 180px;
                    width: 147px;
                  " />
                            <div class="text-center" style="
                    margin-left: 7px;
                    margin-top: -5px;
                    padding-bottom: 1px;
                    width: 156px;
                    background-color: #f4d4d6;
                    border: 1px solid;
                    border-radius: 3px;
                  ">
                                <h6>Adinda Olivia R.</h6>
                                <h6 style="
                      font-size: smaller;
                      margin-top: -7px;
                      margin-bottom: 3px;
                    ">
                                    (PM)
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md" style="margin-left: -100px; margin-right: -100px">
                        <div style="
                  width: 171px;
                  height: 249px;
                  border: 1.5px solid;
                  border-radius: 10px;
                  background-color: #fcebec;
                ">
                            <img src="img/Afif.png" style="
                    border-radius: 5px;
                    margin: 12px;
                    height: 180px;
                    width: 147px;
                  " />
                            <div class="text-center" style="
                    margin-left: 7px;
                    margin-top: -5px;
                    padding-bottom: 1px;
                    width: 156px;
                    background-color: #f4d4d6;
                    border: 1px solid;
                    border-radius: 3px;
                  ">
                                <h6>Afif Dwi Laksono</h6>
                                <h6 style="
                      font-size: smaller;
                      margin-top: -7px;
                      margin-bottom: 3px;
                    ">
                                    (Analyst)
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md">
                        <div style="
                  width: 171px;
                  height: 249px;
                  border: 1.5px solid;
                  border-radius: 10px;
                  background-color: #fcebec;
                ">
                            <img src="img/Soultan.png" style="
                    border-radius: 5px;
                    margin: 12px;
                    height: 180px;
                    width: 147px;
                  " />
                            <div class="text-center" style="
                    margin-left: 7px;
                    margin-top: -5px;
                    padding-bottom: 1px;
                    width: 156px;
                    background-color: #f4d4d6;
                    border: 1px solid;
                    border-radius: 3px;
                  ">
                                <h6>Soultan Ali Hadji</h6>
                                <h6 style="
                      font-size: smaller;
                      margin-top: -7px;
                      margin-bottom: 3px;
                    ">
                                    (Fullstack Dev.)
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="font-weight: lighter">
                <h4 style="margin-top: 40px; margin-bottom: 30px">Our Motivation</h4>
                <p style="margin-top: 30px">
                    &emsp;&emsp;&emsp;Klaim Cat Lovers seharusnya tidak bisa dipakai oleh pecinta kucing ras. Karena mereka hanya merawat kucing ketika ras kucing tersebut unik, mahal, bergengsi dan tentunya harus lucu. Banyak owner-owner tidak bertanggung jawab diluar sana yang membuang kucing dengan berbagai alasan, seperti : sudah tidak lucu lagi, kucing yang sakit-sakitan, kucing yang sudah tua. Dengan adanya website ini kami ingin membuka pikiran para Cat Lovers agar mau mencintai kucing tanpa melihat ras. Bahwa kucing liar pun akan terlihat indah dan terawat bila kita pandai merawatnya. Jangan hanya mau merawat kucing ketika terlihat lucu saja.
                </p>
            </div>
        </div>
    </div>
    <footer class="text-muted">
        <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
            <p>
                Follow us on:
            </p>
            <div style="margin-top: -36px; margin-left: 90px;">
                <p style="font-size: smaller;">
                    <img src="img/ig.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    @hellocat_rescue
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/gmail.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    hellocat.rescue@gmail.com
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="img/yt.png" style="width: 20px; height: 20px; margin-right: 5px;">
                    HelloCat Rescue
                </p>
            </div>
            <p style="
            text-align: center;
          ">
                @Designed & built by SweetBonanza Team
            </p>
        </div>
    </footer>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script></script>
</body>

</html>