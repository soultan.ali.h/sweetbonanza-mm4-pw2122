<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="{{ asset('img/HelloCat Icon.png') }}" />
    <title>Kucing {{$adopt->jenis}}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
    <link href="{{ asset('node_modules\bootstrap\dist\css\bootstrap.min.css') }}" rel="stylesheet" />
    <style>
        input,
        .nav {
            text-decoration: none;
            margin-left: 694px;
            margin-top: 15px;
        }

        a {
            color: black;
        }

        a:hover {
            text-decoration: none;
            color: white;
        }

        .explore:hover {
            color: white;
        }

        .navnav {
            list-style-type: none;
            float: left;
            margin-left: 25px;
        }

        .navbutton:hover {
            background-color: white;
        }

        .disabled {
            pointer-events: none;
            cursor: default;
        }

        .button {
            color: white;
            background-color: #f0a5a9;
        }

        .button:hover {
            background-color: #f09ea2;
        }
    </style>
</head>

<body style="
      background-image: url({{ asset('img/About\ Us_Adopt\ Cat\ Detail\ Background.png') }});
      background-size: 100%;
      background-repeat: no-repeat;
      font-family: 'Roboto', sans-serif;
      background-color: #fbeff0;
    ">
    <div class="nav">
        <ul>
            <li class="navnav"><a href="{{ route('home.index') }}">Home</a></li>
            <li class="navnav"><a href="{{ route('adopt.index') }}">Adopt Cat</a></li>
            <li class="navnav"><a href="{{ route('donation.index') }}">Donation</a></li>
            <li class="navnav"><a href="{{ route('aboutus.index') }}">About Us</a></li>
            <li class="navnav">
                <a href="{{ route('logout') }}"><button class="navbutton" style="
            border-radius: 25px;
            margin-top: -5px;
            border: 1.2px solid;
            width: 90px;
            height: 30px;
            ">
                        Log Out
                    </button></a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div id="content" style="
          padding-top: 40px;
          padding-bottom: 40px;
          width: 88%;
          margin: auto;
          padding-left: 30px;
          padding-right: 30px;
          background-color: white;
          border-radius: 10px;
          margin-top: 8px;
          margin-bottom: 15px;
        ">
            <div style="font-weight: lighter">
                <div class="row">
                    <div class="col-md" style="text-align: center">
                        <img src="{{url('')}}/{{$adopt->foto}}" style="
                  width: 330px;
                  height: 200px;
                  border: 1.5px solid;
                  border-radius: 20px;
                  margin-bottom: 20px;
                " />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md" style="text-align: center; margin-bottom: 40px">
                        <h5>{{$adopt->jenis}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <div style="
                  width: 917px;
                  border: 1.5px solid;
                  border-radius: 10px;
                  background-color: #fbeff0;
                  height: 471px;
                ">
                            <div>
                                <div class="row" style="margin-left: 15px; margin-top: 15px">
                                    <div>
                                        <label>Estimasi Umur Kucing</label>
                                    </div>
                                    <div style="margin-left: 20px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px">
                                        <label>{{$adopt->umur}}</label>
                                    </div>
                                    <div style="margin-left: 479px">
                                        <label>Kondisi</label>
                                    </div>
                                    <div style="margin-left: 20px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px">
                                        <label>{{$adopt->kondisi == 'T' ? 'Tidak Sehat' : 'Sehat'}}</label>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: 15px">
                                    <div>
                                        <label>Pemilik</label>
                                    </div>
                                    <div style="margin-left: 78px">
                                        <label>:</label>
                                    </div>
                                    <div style="margin-left: 5px">
                                        <label>{{$adopt->pemilik}}</label>
                                    </div>
                                </div>
                            </div>
                            <div style="
                    width: 883px;
                    border-radius: 10px;
                    background-color: #f8e4e5;
                    height: 320px;
                    margin-top: 15px;
                    margin-left: 15px;
                  ">
                                <div style="padding: 15px">
                                    <label>Detail</label>
                                    <br /><br />
                                    <p>
                                        {{$adopt->detail == '' ? 'N/A' : $adopt->detail}}
                                    </p>
                                </div>
                            </div>
                            <div>
                                <label style="margin-top: 15px; margin-left: 598px">Contact the owner on</label>
                                <label style="margin-left: 20px">:</label>
                                <label style="margin-left: 5px">{{$adopt->kontak}}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="text-muted">
        <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
            <p>
                Follow us on:
            </p>
            <div style="margin-top: -36px; margin-left: 90px;">
                <p style="font-size: smaller;">
                    <img src="{{ asset('img/ig.png') }}" style="width: 20px; height: 20px; margin-right: 5px;">
                    @hellocat_rescue
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="{{ asset('img/gmail.png') }}" style="width: 20px; height: 20px; margin-right: 5px;">
                    hellocat.rescue@gmail.com
                </p>
                <p style="margin-top: -10px; font-size: smaller;">
                    <img src="{{ asset('img/yt.png') }}" style="width: 20px; height: 20px; margin-right: 5px;">
                    HelloCat Rescue
                </p>
            </div>
            <p style="
            text-align: center;
          ">
                @Designed & built by SweetBonanza Team
            </p>
        </div>
    </footer>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script></script>
</body>

</html>