<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="img/HelloCat Icon.png" />
        <title>HelloCat</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap" rel="stylesheet" />
        <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
        <style>
            input,
            .nav {
                text-decoration: none;
                margin-left: 694px;
                margin-top: 15px;
            }

            a {
                color: black;
            }

            a:hover {
                text-decoration: none;
                color: white;
            }

            .explore:hover {
                color: white;
            }

            .navnav {
                list-style-type: none;
                float: left;
                margin-left: 25px;
            }

            .navbutton:hover {
                background-color: white;
            }

            .disabled {
                pointer-events: none;
                cursor: default;
            }

            .header {
                margin-top: 50px;
            }

            .button {
                color: white;
                background-color: #f0a5a9;
            }

            .button:hover {
                background-color: #f09ea2;
            }
        </style>
    </head>

    <body style="background-image: url(img/Adopt\ Cat\ Background.png); background-size: 100%; background-repeat: no-repeat; font-family: 'Roboto', sans-serif; background-color: #fbeff0;">
        <div class="nav">
            <ul>
                <li class="navnav"><a href="home">Home</a></li>
                <li class="navnav" style="text-decoration: underline; font-size: larger; margin-top: -4px;">
                    <a class="disabled" href="" style="color: black">Adopt Cat</a>
                </li>
                <li class="navnav"><a href="donation">Donation</a></li>
                <li class="navnav"><a href="aboutus">About Us</a></li>
                <li class="navnav">
                    <a href="{{ route('logout') }}">
                        <button class="navbutton" type="button" style="border-radius: 25px; margin-top: -5px; border: 1.2px solid; width: 90px; height: 30px;">
                            Log Out
                        </button>
                    </a>
                </li>
            </ul>
        </div>
        <div class="container">
            <div class="header">
                <h1 style="font-weight: bolder">Taking Care A Homeless Cat</h1>
                <h1 style="margin-left: 425px; margin-top: 20px; font-weight: bolder">
                    Means A Lot
                </h1>
            </div>
            <div id="content" style="padding-top: 40px; padding-bottom: 40px; width: 88%; margin: auto; padding-left: 30px; padding-right: 30px; background-color: white; border-radius: 10px; margin-top: 84px; margin-bottom: 15px;">
                <div style="font-weight: lighter">
                    <div class="row">
                        <div class="col-md">
                            <div style="width: 917px; border: 1.5px solid; border-radius: 10px; background-color: #fcebec; height: 315px;">
                                <h5 style="margin-top: 15px; margin-left: 15px; text-decoration: underline; text-underline-offset: 3px; text-decoration-thickness: 1.5px; font-weight: bold;">
                                    Open Adopt
                                </h5>
                                <div>
                                    <form action="{{ route('adopt.store') }}" method="POST" enctype="multipart/form-data"> @csrf
                                        <div class="row" style="margin-left: 15px; margin-top: 25px">
                                            <div>
                                                <label for="pemilik">Nama Pemilik</label>
                                            </div>
                                            <div style="margin-left: 25px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('pemilik') is-invalid @enderror" id="pemilik" name="pemilik" value="{{ old('pemilik') }}"/> @error('pemilik')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>

                                            <div style="margin-left: 37px">
                                                <label>Kondisi</label>
                                            </div>
                                            <div style="margin-left: 20px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-top: -9px; margin-left: 5px">
                                                <input type="checkbox" style="margin-left: 10px" name="kondisi" id="sehat" value="S" {{ old('kondisi')=='S' ? 'checked': '' }}/>
                                            </div>
                                            <div style="margin-left: 5px">
                                                <label for="sehat">Sehat</label>
                                            </div>

                                            <div style="margin-left: 77px">
                                                <label for="foto">Foto kucing</label>
                                            </div>
                                            <div>
                                                <label>:</label>
                                            </div>
                                            <div style="margin-top: -15px; margin-left: 5px;">
                                                <input type="file" style=" margin-left: 10px; font-size: small; width: 192px;" class="@error('foto') is-invalid @enderror" id="foto" name="foto" value="{{ old('foto') }}"/> @error('foto')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px">
                                            <div>
                                                <label for="jenis">Jenis Kucing</label>
                                            </div>
                                            <div style="margin-left: 33px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('jenis') is-invalid @enderror" id="jenis" name="jenis" value="{{ old('jenis') }}"/> @error('jenis')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>

                                            <div style="margin-top: -9px; margin-left: 117px">
                                                <input type="checkbox" style="margin-left: 10px" name="kondisi" id="tidak_sehat" value="T" {{ old('kondisi')=='T' ? 'checked': '' }}/>
                                            </div>
                                            <div style="margin-left: 5px">
                                                <label for="tidak_sehat">Tidak sehat</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px">
                                            <div>
                                                <label for="umur">Estimasi Umur</label>
                                            </div>
                                            <div style="margin-left: 20px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ old('umur') }}"/> @error('umur')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>

                                            <div style="margin-left: 37px">
                                                <label for="kontak">Kontak</label>
                                            </div>
                                            <div style="margin-left: 22px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: -13px">
                                                <input type="text" placeholder="" style="margin-left: 10px; height: 20px; border-radius: 5px; border: 1px solid; font-weight: lighter;" class="@error('kontak') is-invalid @enderror" id="kontak" name="kontak" value="{{ old('kontak') }}"/> @error('kontak')
                                                <div class="text-danger">{{ $message }}</div> @enderror
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left: 15px">
                                            <div>
                                                <label for="detail">Detail</label>
                                            </div>
                                            <div style="margin-left: 84px">
                                                <label>:</label>
                                            </div>
                                            <div style="margin-left: 5px; margin-top: 3px">
                                                <textarea placeholder="ceritakan tentang kucing kamu.." style="margin-left: 10px; height: 70px; width: 742px; border-radius: 5px; border: 1px solid; font-weight: lighter;" id="detail" rows="3" name="detail">{{ old('detail') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 25px; margin-left: 818px">
                                            <div>
                                                <a href="">
                                                    <button type="submit" class="button" style="border: 0px solid; border-radius: 25px; width: 80px; height: 30px;">
                                                        Submit
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <h4 style="margin-top: 40px; margin-bottom: 30px;">
                        Available Cat
                    </h4>
                    <div class="row" style="margin-top: 10px; margin: auto; margin-left: 15px;">
                    @foreach($adopts as $adopsi)
                        <div class="col-md" style="margin-top: 30px;">
                            <a href="{{ route('adopt.show',['adopt' => $adopsi->id]) }}">
                                <div class="card" style="width: 16rem; border: 1.5px solid; border-radius: 10px">
                                    <img src="{{url('')}}/{{$adopsi->foto}}" class="card-img-top" style="height: 150px; border-top-left-radius: 10px; border-top-right-radius: 10px;" />
                                    <div class="card-body" style="background-color: #f0a5a9; height: 30px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
                                        <p class="card-text" style="margin-top: -10px">{{$adopsi->jenis}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
                <div class="row" style="margin-top: 10px; margin: auto; margin-left: 15px;">
                </div>
            </div>
        </div>
        </div>
        <footer class="text-muted">
            <div style="background-color: #f4d4d6; font-size: smaller; color: black; height: 125px; padding: 15px;">
                <p>
                    Follow us on:
                </p>
                <div style="margin-top: -36px; margin-left: 90px;">
                    <p style="font-size: smaller;">
                        <img src="img/ig.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        @hellocat_rescue
                    </p>
                    <p style="margin-top: -10px; font-size: smaller;">
                        <img src="img/gmail.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        hellocat.rescue@gmail.com
                    </p>
                    <p style="margin-top: -10px; font-size: smaller;">
                        <img src="img/yt.png" style="width: 20px; height: 20px; margin-right: 5px;">
                        HelloCat Rescue
                    </p>
                </div>
                <p style="
                    text-align: center;
                ">
                    @Designed & built by SweetBonanza Team
                </p>
            </div>
        </footer>
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script></script>
    </body>

</html>