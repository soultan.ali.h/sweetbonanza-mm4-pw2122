<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="static\HelloCat Icon.png" />
        <title>SweetBonanza</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet" />
        <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
        <style>
            .containersm {
                width: 60%;
            }

            .exceptname {
                width: 500px;
            }

            .onhover:hover {
                font-size: smaller;
            }

            button {
                color: white;
                background-color: #f0a5a9;
            }

            button:hover {
                background-color: #f09ea2;
            }
        </style>
    </head>

    <body style="background-image: url(img/Sign\ In\ Page\ Background.png); background-size: 100%; font-family: 'Roboto', sans-serif;">
        <div class="container">
            <div class="row" style="margin-top: 125px">
                <div class="col-md text-center">
                    <h1>SIGN IN</h1>
                </div>
            </div>
            <div class="containersm" style="margin: auto">
                <form action="{{ route('login') }}" method="POST"> @csrf
                    <div class="row" style="margin-top: 70px; margin-left: 68px; font-size: smaller">
                        <div class="col-md">
                            <p for="email">Email</p>
                            <input class="exceptname" type="text" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" class="@error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}"> @error('email')
                            <div class="text-danger">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="row" style="margin-top: 35px; margin-left: 68px; font-size: smaller">
                        <div class="col-md">
                            <p>Password</p>
                            <input class="exceptname" type="password" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" class="@error('password') is-invalid @enderror" id="password" name="password" value="{{ old('password') }}"> @error('password')
                            <div class="text-danger">{{ $message }}</div> @enderror
                        </div>
                    </div>
                    <div class="row" style="margin-top: 40px; font-size: smaller">
                        <div class="col-md text-center">
                            <a href="">
                                <button type="submit" style="border: 0px solid; border-radius: 25px; width: 80px; height: 40px;">
                                    Sign In
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px; font-size: smaller">
                        <div class="col-md text-center">
                            <label>
                                <p class="onhover">
                                    <a href="register" style="font-size: larger; text-decoration: none; color: black">Click here</a>
                                </p>
                            </label>
                            <label>
                                <p>if you don't have an account yet</p>
                            </label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <footer class="text-muted"></footer>
        <script src="node_modules\jquery\dist\jquery.min.js"></script>
        <script src="node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
        <script></script>
    </body>

</html>