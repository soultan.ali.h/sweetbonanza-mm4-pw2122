<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="/img/HelloCat Icon.png" />
    <title>SweetBonanza</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet" />
    <link href="node_modules\bootstrap\dist\css\bootstrap.min.css" rel="stylesheet" />
    <style>
        .containersm {
            width: 60%;
        }

        .name {
            width: 300px;
        }

        .exceptname {
            width: 500px;
        }

        .onhover:hover {
            font-size: smaller;
        }

        button {
            color: white;
            background-color: #f0a5a9;
        }

        button:hover {
            background-color: #f09ea2;
        }
    </style>
</head>

<body style="
      background-image: url(img/Sign\ Up\ Page\ Background.png);
      background-size: 100%;
      font-family: 'Roboto', sans-serif;
    ">
    <div class="container">
        <div class="row" style="margin-top: 30px">
            <div class="col-md text-center">
                <h1>SIGN UP</h1>
            </div>
        </div>
        <form action="{{ route('register') }}" method="POST"> @csrf
            <div class="containersm" style="margin: auto">
                <div class="row" style="margin-top: 50px; font-size: smaller">
                    <div class="col-md">
                        <p for="first">First Name</p>
                        <input class="name @error('first') is-invalid @enderror" type="text" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" placeholder="type first name" id="first" name="first" value="{{ old('first') }}"> @error('first')
                        <div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                    <div class="col-md">
                        <p for="last">Last Name</p>
                        <input class="name @error('last') is-invalid @enderror" type="text" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" placeholder="type last name" id="last" name="last" value="{{ old('last') }}"> @error('last')
                        <div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="row" style="margin-top: 35px; font-size: smaller">
                    <div class="col-md">
                        <p for="email">Email</p>
                        <input class="exceptname @error('email') is-invalid @enderror" type="email" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" placeholder="type email" id="email" name="email" value="{{ old('email') }}"> @error('email')
                        <div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="row" style="margin-top: 35px; font-size: smaller">
                    <div class="col-md">
                        <p for="password">Password</p>
                        <input class="exceptname @error('password') is-invalid @enderror" type="password" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" placeholder="type password" id="password" name="password" value="{{ old('password') }}"> @error('password')
                        <div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="row" style="margin-top: 35px; font-size: smaller">
                    <div class="col-md">
                        <p for="password">Confirm Password</p>
                        <input class="exceptname @error('password_confirmation') is-invalid @enderror" type="password" style="border: none; border-bottom: 1.5px solid; background-color: transparent;" placeholder="confirm password" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}"> @error('password_confirmation')
                        <div class="text-danger">{{ $message }}</div> @enderror
                    </div>
                </div>
                <div class="row" style="margin-top: 35px; font-size: smaller">
                    <div class="col-md text-center">
                        <a href="signin.html">
                            <button type="submit" style="border: 0px solid; border-radius: 25px; width: 80px; height: 40px;">
                                Sign Up
                            </button>
                        </a>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px; font-size: smaller">
                    <div class="col-md text-center">
                        <label>
                            <p class="onhover">
                                <a href="login" style="font-size: larger; text-decoration: none; color: black">Click here</a>
                            </p>
                        </label>
                        <label>
                            <p>if you already have an account</p>
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <footer class="text-muted"></footer>
    <script src="node_modules\jquery\dist\jquery.min.js"></script>
    <script src="node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
    <script></script>
</body>

</html>