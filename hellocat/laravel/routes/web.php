<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'LandingController@index')->name('landing.index');
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

Route::middleware(['auth'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home.index');

    Route::post('/adopt', 'AdoptController@store')->name('adopt.store');
    Route::get('/adopt', 'AdoptController@index')->name('adopt.index');
    Route::get('/adopt/{adopt}', 'AdoptController@show')->name('adopt.show');
    Route::delete('/adopt/{adopt}', 'AdoptController@destroy')->name('adopt.destroy');

    Route::post('/donation', 'DonationController@store')->name('donation.store');
    Route::get('/donation', 'DonationController@index')->name('donation.index');
    Route::get('/donation/{donation}', 'DonationController@show') ->name('donation.show');
    Route::delete('/donation/{donation}', 'DonationController@destroy')->name('donation.destroy');

    Route::get('/aboutus', 'AboutUsController@index')->name('aboutus.index');
});

Route::get('/admin', 'AdminController@index')->name('admin.index');
Auth::routes();