<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Adopt;
use App\Donation;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $adopsis = Adopt::latest()->get();
        $donasis = Donation::latest()->get();
        if(auth()->user()->role == 'admin') {
            return view('admin.index',['adopts' => $adopsis], ['donations' => $donasis]);
        } else {
            return redirect()->route('home.index');
        }
    }
}
