<?php

namespace App\Http\Controllers;

use App\Adopt;
use App\Donation;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        $adopsis = Adopt::latest()->take(3)->get();
        $donasis = Donation::latest()->take(1)->get();
        return view('landing.index',['adopts' => $adopsis],['donations' => $donasis]);
    }
}
