<?php

namespace App\Http\Controllers;

use App\Adopt;
use Illuminate\Http\Request;

class AdoptController extends Controller
{
    public function store(Request $request)
    {
        $validateData = $request->validate
        ([
            'pemilik' => 'required|min:3|max:50',
            'jenis' => 'required|min:3|max:50',
            'umur' => 'required|min:1|max:3',
            'kondisi' => 'required|in:S,T',
            'kontak' => 'required|min:12|max:15',
            'foto' => 'required|file|image|max:10000',
            'detail' => '',
        ]);
        
        $adopsi = new Adopt();
        $adopsi->pemilik = $validateData['pemilik'];
        $adopsi->jenis = $validateData['jenis'];
        $adopsi->umur = $validateData['umur'];
        $adopsi->kondisi = $validateData['kondisi'];
        $adopsi->kontak = $validateData['kontak'];
        if($request->hasFile('foto'))
        {
            $extFile = $request->foto->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->foto->move('assets/images',$namaFile);
            $adopsi->foto = $path;
        }
        $adopsi->detail = $validateData['detail'];
        $adopsi->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('adopt.index');
    }

    public function index()
    {
        $adopsis = Adopt::latest()->take(9)->get();
        return view('adopt.index',['adopts' => $adopsis]);
    }

    public function show($id)
    {
        $result = Adopt::findOrFail($id);
        return view('adopt.show',['adopt' => $result]);
    }

    public function destroy(Request $request, Adopt $adopt)
    {
        $adopt->delete();
        return redirect()->route('admin.index');
    }
}