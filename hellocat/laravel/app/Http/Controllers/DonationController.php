<?php

namespace App\Http\Controllers;

use App\Donation;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class DonationController extends Controller
{
    public function store(Request $request)
    {
        $validateData = $request->validate
        ([
            'instansi' => 'required|min:1|max:50',
            'rekening' => 'required|min:1|max:50',
            'bank' => 'required|min:1|max:50',
            'nama' => 'required|min:1|max:50',
            'kucing' => 'required|file|image|max:10000',
            'ktp' => 'required|file|image|max:10000',
            'detail' => '',         
        ]);
        
        $donasi = new Donation();
        $donasi->instansi = $validateData['instansi'];
        $donasi->rekening = $validateData['rekening'];
        $donasi->bank = $validateData['bank'];
        $donasi->nama = $validateData['nama'];
        if($request->hasFile('kucing'))
        {
            $extFile = $request->kucing->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->kucing->move('assets/images',$namaFile);
            $donasi->kucing = $path;
        }
        if($request->hasFile('ktp'))
        {
            $extFile = $request->ktp->getClientOriginalExtension();
            $namaFile = 'user-'.time().".".$extFile;
            $path = $request->ktp->move('assets/images',$namaFile);
            $donasi->ktp = $path;
        }
        $donasi->detail = $validateData['detail'];
        $donasi->save();
        $request->session()->flash('pesan','Penambahan data berhasil');
        return redirect()->route('donation.index');
    }

    public function index()
    {
        $donasis = Donation::latest()->paginate(1);
        Paginator::useBootstrapThree();
        return view('donation.index', ['donations' => $donasis]);
    }

    public function show($id)
    {
        $result = Donation::findOrFail($id);
        return view('donation.show',['donation' => $result]);
    }

    public function destroy(Request $request, Donation $donation)
    {
        $donation->delete();
        return redirect()->route('home.index');
    }
}
