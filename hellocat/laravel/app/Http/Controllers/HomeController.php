<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adopt;
use App\Donation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $adopsis = Adopt::latest()->take(3)->get();
        $donasis = Donation::latest()->take(1)->get();
        return view('home.index',['adopts' => $adopsis],['donations' => $donasis]);
    }
}
